FROM ruby:2.5.3

ADD . .

ENV BUNDLE_PATH=/home/web/app/vendor/bundle
ENV BUNDLER_VERSION="1.16.2"

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install

CMD ["bundle", "exec" ,"ruby", "fetcher.rb"]
