require 'net/http'
require 'zlib'
require 'rubygems'
require 'rchardet'

class Fetcher
  def call(params)
    url = params[:url]

    Dir.chdir('tmp')

    encode_to_utf(extract_file(download_file(url)))
  end

  private

  def download_file(url)
    uri = URI(url)

    begin
      tempfile = File.open(File.basename(url), 'w')
      Net::HTTP.start(uri.host, uri.port, use_ssl: (uri.scheme == 'https')) do |http|
        request = Net::HTTP::Get.new(uri)
        request['User-Agent'] = 'Mozilla/5.0'

        http.request(request) do |response|
          response.read_body do |chunk|
            tempfile.write(chunk)
          end
        end
      end
    ensure
      tempfile.close
    end
    tempfile
  end

  def extract_file(file)
    extension = File.extname(file)
    return file unless %w[.gz .zip].include?(extension)

    begin
      tempfile = File.open(File.basename(file, extension).to_s, 'w')
      Zlib::GzipReader.open(file) do |source|
        while (chunk = source.read(500_000))
          tempfile.write(chunk)
        end
      end
    ensure
      tempfile.close
    end
    tempfile
  end

  def encode_to_utf(file)
    encoding = detect_encoding(file)
    return file if encoding == 'utf-8'
    dest = File.open("utf-8-#{File.basename(file)}", 'w')

    File.open(file, "r:#{encoding}:utf-8") do |f|
      dest.write(f.readline(1024)) until f.eof?
    end
    dest.close
    FileUtils.mv(dest, File.basename(file))
    file
  end

  def detect_encoding(file)
    sample_string = nil
    File.open(file, 'r') { |f| sample_string = f.read(100) }
    CharDet.detect(sample_string)['encoding']
  end
end
