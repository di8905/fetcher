require 'webmock/rspec'
require 'fetcher'

WebMock.disable_net_connect!(allow_localhost: true)

RSpec.describe 'it processes link' do
  let(:url) { 'https://res.cloudinary.com/shoppilot/raw/upload/v1536059472/challenges/war-and-peace.txt.gz' }
  let(:file) { File.open('spec/war-and-peace.txt.gz', 'r') }
  let(:search_string) { 'Я пишу до сих пор только о князьях, графах, министрах, сенаторах' }

  it 'processes the file' do
    stub_request(:get, url).to_return(status: 200, body: file)

    result = Fetcher.new.call(url: url)

    expect(File.open(result).readline).to include(search_string)
  end
end
